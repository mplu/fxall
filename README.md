# fxall



## Getting started

Welcome to FXAll autopilot !
Written by Kerbals, for Kerbals ! (Means : do a quicksave before)

To install it, copy the two files and the folder in the <KSP install folder>/Ships/Scripts

## How to start it 

To start it, you can either select it as a boot file in the VAB/SPH, or start it via the embedded console using :
- switch to 0.
- run fxall.

## How to use it 

### Configurable
The main window is mainly which orbit you want to go :
- 110 km
- 500 km
- 2 863.33 km
- custom : You need to choose one option. If custom is chosen, the value that will be use is the value just below, in the field "custom altitude"

Launch Option :
* Engine Already activated | 
 When you click on the "launch !" button, it will stage to ignite motor. If motor is already started, using this option avoid the stage operation (typically after you land your ship)
* Check motor before release  | 
Sometime, solid rocket booster fail to start (typically, when using Kerbalism, it happens). To avoid asymmetrical thrust on lift of, a check can be performed before releasing the TT18-A Launch Stability Enhancer. In that case, the first stage should be SRB, next stage the LSE. If the measured thrust does correspond to the theorical thrust of the stage, the LSE is released.
* Autostage | 
Enable automatic staging.
* Direction | 
Direction of launch

### Buttons
* Launch | 
It launch the rocket and circulaze at the configured altitude. A TWR of more than 1.5 is recommanded
* Execute next node | 
If a node is present, the manoeuvre will be executed automatically.
* Autoland | 
Land automatically, TWR of 3 is recommanded
* Panic Reboot | 
Stop everything and reboot the kOS computer. Can be use to quite a ongoing procedure.
