global starttime is time:seconds.

runPath("0:/lib_str_to_num.ks").

function mlog {
	parameter text.
	set timestamp to ROUND(time:seconds - starttime,3).
	print timestamp + " " + text.
	LOG timestamp + " " + text to mylog.txt.
}

function AutoLand {
	PRINT "Landing routine enabled".


	WAIT UNTIL SHIP:VERTICALSPEED < 0.
	PRINT "Début de la descente".

	WAIT UNTIL ALT:RADAR < 13500.
	PRINT "Début de la manoeuvre, verouillage du cap".
	LOCK STEERING to (-1)* SHIP:VELOCITY:SURFACE.
	LOCK twr to (maxthrust / mass / 9.89) .
	SAS OFF.

	WAIT UNTIL ALT:RADAR < 13000.

	PRINT "mise en route correction, twr = " + twr.

	LOCK consigne_vitesse to (0.0717*ALT:RADAR+83)/2.
	LOCK altitude TO SHIP:ALTITUDE.
	SET Kp to .4.

	lock vitesse TO SHIP:VELOCITY:SURFACE:MAG.
	LOCK dthrott TO Kp * ( vitesse - consigne_vitesse).

	SET thrott TO 0.
	LOCK THROTTLE to thrott.



	UNTIL ALT:RADAR < 1000 {
		SET thrott to dthrott.
		print "1 thrott:" + round(thrott,3) + " setpoint:" + round(consigne_vitesse,3).
		WAIT 0.1.
	}
	LOCK consigne_vitesse to (-0.000075 * ALT:RADAR*ALT:RADAR + 0.2292*ALT:RADAR + 0.08977)/2.
	gear on.
	AG10 on.

	// measure height of craft
	list parts in partList.
	set lp to 0.//lowest part height
	set hp to 0.//hightest part height
	for p in partList{
		set cp to facing:vector * p:position.
		if cp < lp
			set lp to cp.
		else if cp > hp
			set hp to cp.
	}
	set height to hp - lp.
	PRINT "Craft size is " + height.

	UNTIL ALT:RADAR < height + 1 {
	//until STATUS = LANDED {
		SET thrott to dthrott.
		print "4 thrott:" + round(thrott,3) + " setpoint:" + round(consigne_vitesse,3).
		WAIT 0.1.
	}
	print "Fin d'asservissement de vitesse".
	LOCK THROTTLE to 0.
	SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
	LIST ENGINES IN myVariable.
	FOR eng IN myVariable {
		print "Engine " + eng:NAME+ " with ISP = " + eng:ISP + " will shutdown".
		eng:SHUTDOWN().
		print "	ISP is now = " + eng:ISP.
	}.
	UNLOCK STEERING.
	sas on.
	FROM {local countdown is 5.} UNTIL countdown = 0 STEP {SET countdown to countdown - 1.} DO {
		PRINT "Stabilizing ... " + countdown.
		WAIT 1. // pauses the script here for 1 second.
	}
	PRINT "Landed".
	UNLOCK THROTTLE.

}

function ExecNextNode {
	// Imported from KOS Tutorial
	// https://ksp-kos.github.io/KOS_DOC/tutorials/exenode.html
	sas off.
	set nd to nextnode.
	print "Node in: " + round(nd:eta) + ", DeltaV: " + round(nd:deltav:mag).

	//calculate ship's max acceleration
	set max_acc to ship:maxthrust/ship:mass.

	// Now we just need to divide deltav:mag by our ship's max acceleration
	// to get the estimated time of the burn.
	//
	// Please note, this is not exactly correct.  The real calculation
	// needs to take into account the fact that the mass will decrease
	// as you lose fuel during the burn.  In fact throwing the fuel out
	// the back of the engine very fast is the entire reason you're able
	// to thrust at all in space.  The proper calculation for this
	// can be found easily enough online by searching for the phrase
	//   "Tsiolkovsky rocket equation".
	// This example here will keep it simple for demonstration purposes,
	// but if you're going to build a serious node execution script, you
	// need to look into the Tsiolkovsky rocket equation to account for
	// the change in mass over time as you burn.
	//
	set burn_duration to nd:deltav:mag/max_acc.
	print "Crude Estimated burn duration: " + round(burn_duration) + "s".
	wait until nd:eta <= (burn_duration/2 + 60).

	set np to nd:deltav. //points to node, don't care about the roll direction.
	lock steering to np.

	//now we need to wait until the burn vector and ship's facing are aligned
	wait until vang(np, ship:facing:vector) < 0.25.

	//the ship is facing the right direction, let's wait for our burn time
	wait until nd:eta <= (burn_duration/2).

	//we only need to lock throttle once to a certain variable in the beginning of the loop, and adjust only the variable itself inside it
	set tset to 0.
	lock throttle to tset.

	set done to False.
	//initial deltav
	set dv0 to nd:deltav.
	until done
	{
		//recalculate current max_acceleration, as it changes while we burn through fuel
		set max_acc to ship:maxthrust/ship:mass.

		//throttle is 100% until there is less than 1 second of time left to burn
		//when there is less than 1 second - decrease the throttle linearly
		set tset to min(nd:deltav:mag/max_acc, 1).

		//here's the tricky part, we need to cut the throttle as soon as our nd:deltav and initial deltav start facing opposite directions
		//this check is done via checking the dot product of those 2 vectors
		if vdot(dv0, nd:deltav) < 0
		{
			print "End burn, remain dv " + round(nd:deltav:mag,1) + "m/s, vdot: " + round(vdot(dv0, nd:deltav),1).
			lock throttle to 0.
			break.
		}

		//we have very little left to burn, less then 0.1m/s
		if nd:deltav:mag < 0.1
		{
			print "Finalizing burn, remain dv " + round(nd:deltav:mag,1) + "m/s, vdot: " + round(vdot(dv0, nd:deltav),1).
			//we burn slowly until our node vector starts to drift significantly from initial vector
			//this usually means we are on point
			wait until vdot(dv0, nd:deltav) < 0.5.

			lock throttle to 0.
			print "End burn, remain dv " + round(nd:deltav:mag,1) + "m/s, vdot: " + round(vdot(dv0, nd:deltav),1).
			set done to True.
		}
	}
	unlock steering.
	unlock throttle.
	wait 1.

	//we no longer need the maneuver node
	remove nd.

	//set throttle to 0 just in case.
	SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
}

set terminal:width to 45.
set terminal:height to 45.
clearScreen.
core:part:getmodule("kosProcessor"):doEvent("open terminal").

SET mygui TO GUI(50).

SET lbl_gui_title TO mygui:ADDLABEL("Welcome to kOS Autopilot fxall").
SET lbl_shipname TO mygui:ADDLABEL("Ship : " + ship:name).
SET lbl_lauchalt_config TO mygui:ADDLABEL("Launch orbit selection").

SET btn_acti_lko TO mygui:ADDRADIOBUTTON("Low Orbit : 110km", true).
SET btn_acti_mko TO mygui:ADDRADIOBUTTON("Medium Orbit : 500km", false).
SET btn_acti_gko TO mygui:ADDRADIOBUTTON("Kerbin Geosync Orb : 2863,33km", false).
//SET bx_custorb TO mygui:ADDHLAYOUT().
SET btn_acti_co TO mygui:ADDRADIOBUTTON("Custom Orbit (in meter)", false).
SET consigne_custalt TO mygui:ADDTEXTFIELD("85000").
SET bx_direction TO mygui:ADDHLAYOUT().
SET lbl_consigne_custdir TO bx_direction:ADDLABEL("Direction").
SET consigne_custdir TO bx_direction:ADDTEXTFIELD("90").
SET lbl_consigne_custalt TO mygui:ADDLABEL("Option").

SET btn_acti_EngEn TO mygui:ADDCHECKBOX("Engine already activated",false).
SET btn_acti_NeedRel TO mygui:ADDCHECKBOX("Check Motor before releasing",false).
SET btn_acti_Autstage TO mygui:ADDCHECKBOX("Auto Stage",true).

SET bxmaneuvr TO mygui:ADDHLAYOUT().
SET btn_launch TO bxmaneuvr:ADDBUTTON("Launch !").
SET btn_land TO bxmaneuvr:ADDBUTTON("AutoLand").
SET btn_exec TO mygui:ADDBUTTON("Exec next node").
SET bxcreatenode TO mygui:ADDHLAYOUT().
SET lbl_nodecreationtitle TO bxcreatenode:ADDLABEL("Create circ. node:").
SET btn_APcircunode TO bxcreatenode:ADDBUTTON("Ap").
SET btn_PEcircunode TO bxcreatenode:ADDBUTTON("Pe").
SET bxresetquit TO mygui:ADDHLAYOUT().
SET btn_panic TO bxresetquit:ADDBUTTON("Panic Reboot").
SET btn_quit TO bxresetquit:ADDBUTTON("Quit").

mygui:SHOW().
SET mygui:X TO 1250.
SET mygui:Y TO 300.

set OrbitAltTarget to 0.
SET quit_val to 0.

WHEN (btn_panic:pressed = TRUE) THEN {
			SET quit_val to 1.
			myGui:DISPOSE().
			CLEARGUIS().
			reboot.
			preserve.
		}.

set btn_quit:ONCLICK to {
	SET quit_val to 1.
	myGui:DISPOSE().
}.

set btn_APcircunode:ONCLICK to {
	set speed to body:radius * SQRT((Constant:G*body:mass / ( body:radius^2)) / ( SHIP:APOAPSIS + body:radius)).
	SET myNode to NODE( TIME:SECONDS + ETA:APOAPSIS, 0, 0, speed - velocityat(ship,TIME:SECONDS + ETA:APOAPSIS):orbit:mag  ).
	ADD myNode.
}.

set btn_PEcircunode:ONCLICK to {
	set speed to body:radius * SQRT((Constant:G*body:mass / ( body:radius^2)) / ( SHIP:PERIAPSIS + body:radius)).
	SET myNode to NODE( TIME:SECONDS + ETA:PERIAPSIS, 0, 0, speed - velocityat(ship,TIME:SECONDS + ETA:PERIAPSIS):orbit:mag  ).
	ADD myNode.
}.


set btn_launch:ONCLICK to {

	if btn_acti_lko:pressed = TRUE{
		set OrbitAltTarget to 110000.
	}
	if btn_acti_mko:pressed = TRUE{
		set OrbitAltTarget to 500000.
	}
	if btn_acti_gko:pressed = TRUE{
		set OrbitAltTarget to 2863330.
	}
	if btn_acti_co:pressed = TRUE{
		set OrbitAltTarget to str_to_num(consigne_custalt:TEXT).
	}
	set custdir to str_to_num(consigne_custdir:TEXT).

	mlog ("++++++ Flight of " + ship:name + " at " + starttime + "++++++").

	SAS ON.
	set nearOrbitAltTarget to OrbitAltTarget - OrbitAltTarget/10.

	//This is our countdown loop, which cycles from 3 to 0
	mlog ("Counting down:").
	FROM {local countdown is 3.} UNTIL countdown = 0 STEP {SET countdown to countdown - 1.} DO {
		mlog ( "..." + countdown).
		WAIT 1. // pauses the script here for 1 second.
	}
	LOCK THROTTLE TO 1.
	//démarrage
	if btn_acti_EngEn:pressed = FALSE
	{
		STAGE.
	}

	WAIT 0.7. // pauses the script here for 0.5 second.

	//measure actual thrust
	LIST ENGINES IN myVariable.
	set totalthrust to 0.
	set AllEngineStarted to 1.
	FOR eng IN myVariable {
		set totalthrust to totalthrust + eng:thrust.
		mlog ("An engine exists with thrust = " + eng:thrust + " and can be restarted " + eng:ALLOWRESTART).
	}.

	set diff to ship:AVAILABLETHRUST - totalthrust.
	// check thrust
	mlog ( "Total thrust " + totalthrust).
	mlog ( "Available thrust " + ship:AVAILABLETHRUST).
	mlog ( "Diff thrust " + diff).

	IF (ABS(diff) < 10) and (AllEngineStarted = 1) {
		mlog ( "ALL ENGINE RUNNING, lift off").
		if btn_acti_NeedRel:pressed = TRUE
		{
			STAGE.
		}

		WHEN ((STAGE:liquidfuel < 0.1) and (STAGE:solidfuel < 0.1)) THEN {
			if btn_acti_Autstage:pressed = TRUE
			{
				mlog (  "Stage separation").
				wait 0.7.
				STAGE.
			}
			wait 0.7.
			preserve.
		}

		WAIT UNTIL SHIP:ALTITUDE > 1000.
		if(body:atm:exists = TRUE)
		{
			LOCK consigne_angle to (1e-08	* SHIP:ALTITUDE*SHIP:ALTITUDE - 0.0018 *SHIP:ALTITUDE + 82.715).
		//LOCK consigne_angle to (8.0e-18	* SHIP:ALTITUDE ^ 4 - 1E-12* SHIP:ALTITUDE ^ 3 + 9E-08* SHIP:ALTITUDE ^ 2 - 0.0034 * SHIP:ALTITUDE + 88.813 ).
		//y = 8.0e-18x4 - 1E-12x3 + 9E-08x2 - 0,0034x + 86,813
		}else
		{
			LOCK consigne_angle to 5.
		}
		SAS OFF.
		LOCK STEERING TO HEADING(custdir,consigne_angle).
		// LOCK STEERING TO HEADING(custdir,80).
		//mlog ("Alt > 1000, heading 80").
		//UNTIL ((SHIP:APOAPSIS > OrbitAltTarget) or (SHIP:ALTITUDE > 75000)){
		UNTIL ((SHIP:APOAPSIS > OrbitAltTarget) or (consigne_angle < 3)){
		print " Heading to : " + consigne_angle.
		WAIT 0.1.
		}
		AG10 on.
		LOCK consigne_angle to 3.
		UNTIL ((SHIP:APOAPSIS > OrbitAltTarget) ){
		print " Heading to : " + consigne_angle.
		WAIT 0.1.
		}

		LOCK STEERING TO HEADING(custdir,3).
		LOCK THROTTLE TO 0.0.
		mlog (  "APOAPSIS "+OrbitAltTarget+", heading 3 & stopping engine").

		// creating node for Circularization at APOAPSIS
		set speed to body:radius * SQRT((Constant:G*body:mass / ( body:radius^2)) / ( SHIP:APOAPSIS + body:radius)).
		SET myNode to NODE( TIME:SECONDS + ETA:APOAPSIS, 0, 0, speed - velocityat(ship,TIME:SECONDS + ETA:APOAPSIS):orbit:mag  ).
		ADD myNode.

		mlog (  "Prepare to exec node").
		ExecNextNode().
		mlog (  "Circularization done, A :" +SHIP:APOAPSIS + " P : "+ SHIP:PERIAPSIS).

		//set throttle to 0 just in case.
		SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
		SAS ON.
		UNLOCK STEERING.
		UNLOCK THROTTLE.
		mlog (  "End of Guidance").

		} ELSE {
			mlog ( "ENGINE failure !").
			LOCK THROTTLE TO 0.0.   // 1.0 is the max, 0.0 is idle.
			mlog ( "wait for end of burn").
		}

	}.

set btn_exec:ONCLICK to {
	if ADDONS:KAC:AVAILABLE
	{
		set nd to nextnode.
		if (round(nd:eta) > 180)
		{
			set na to addAlarm("Maneuver",time:seconds+round(nd:eta) - 180, ship:name+" Maneuver", "").
		}
	}

	ExecNextNode().
}.

set btn_land:ONCLICK to {
	AutoLand().
}.

until quit_val {


}.
//Lancement

//SET SHIP:CONTROL:NEUTRALIZE TO true.
//UNLOCK THROTTLE.
print "quit.".